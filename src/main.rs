use clap::{App, Arg, ArgMatches};
use log::{info, warn, LevelFilter};
use anyhow::{Result, Context};
use std::fs;
use env_logger::Builder;

fn main() -> Result<()> {
    Builder::new()
        .filter(None, LevelFilter::Info)
        .init();

    let matches = App::new("Rust CLI Tool")
        .version("1.0")
        .author("Hanze Meng")
        .about("Processes data in a directory")
        .arg(Arg::with_name("directory")
            .help("Sets the directory to process")
            .required(true)
            .index(1))
        .arg(Arg::with_name("extension")
            .short("e")
            .long("extension")
            .value_name("EXTENSION")
            .help("Filters files by the given extension")
            .takes_value(true))
        .arg(Arg::with_name("count")
            .short("c")
            .long("count")
            .help("Counts the total number of files in the directory"))
        .get_matches();

    process_arguments(&matches)
}

fn process_arguments(matches: &ArgMatches) -> Result<()> {
    let directory = matches.value_of("directory").unwrap();
    info!("Processing directory: {}", directory);

    if matches.is_present("count") {
        let total_files = count_files(directory)?;
        info!("Total number of files: {}", total_files);
    } else {
        if !matches.is_present("extension") {
            list_files(directory)?;
        }
        if let Some(extension) = matches.value_of("extension") {
            filter_files_by_extension(directory, extension)?;
        }
    }

    Ok(())
}

fn list_files(directory: &str) -> Result<()> {
    let paths = fs::read_dir(directory)
        .with_context(|| format!("Could not read the directory: {}", directory))?;

    for path in paths {
        let path = path?.path();
        println!("{}", path.display());
    }
    Ok(())
}

fn count_files(directory: &str) -> Result<usize> {
    let paths = fs::read_dir(directory)
        .with_context(|| format!("Could not read the directory: {}", directory))?;

    Ok(paths.count())
}

fn filter_files_by_extension(directory: &str, extension: &str) -> Result<()> {
    let paths = fs::read_dir(directory)
        .with_context(|| format!("Could not read the directory: {}", directory))?;

    let mut matched_files = 0;
    let mut skipped_files = 0;

    for path in paths {
        let path = path?.path();
        if let Some(ext) = path.extension() {
            if ext.to_str() == Some(extension) {
                matched_files += 1;
            } else {
                skipped_files += 1;
            }
        }
    }

    info!("Files matching '{}': {}", extension, matched_files);
    warn!("Files skipped: {}", skipped_files);

    Ok(())
}
