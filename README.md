# Rust CLI Tool for Directory Processing

This Rust-based CLI tool is designed to efficiently process data within directories. It provides functionalities to list all files in a specified directory, count the total number of files, and count files matching the provided extension. Developed with a focus on error handling, logging, and comprehensive unit testing, this tool exemplifies practices in Rust development for command-line applications.

## Features

- **File Listing**: List all files within a specified directory.
- **File Counting**: Count the total number of files in the directory.
- **Extension Filtering**: Filter and count the number of files matched/unmatched based on a specific file extension.

## Getting Started

### Prerequisites

Ensure you have Rust and Cargo installed on your system, and the system should be UNIX-like.

### Installation

Clone the repository to your local machine:

```bash
git clone https://gitlab.com/hm222/rust-cli
cd rust-cli
```

Build the project using Cargo:

```bash
cargo build --release
```

### Usage

To list files in a directory:

```bash
./target/release/rust-cli /path/to/directory
```

To count files in a directory:

```bash
./target/release/rust-cli /path/to/directory --count
```
A sample output is shown below:
```bash
[2024-03-23T06:06:54Z INFO  rust_cli] Processing directory: ../../../../<directory>/
[2024-03-23T06:06:54Z INFO  rust_cli] Total number of files: 18
```

To filter files by extension:

```bash
./target/release/rust-cli /path/to/directory --extension <ext>
```
A sample output is shown below:
```bash
[2024-03-23T06:05:36Z INFO  rust_cli] Processing directory: ../../../../<directory>/
[2024-03-23T06:05:36Z INFO  rust_cli] Files matching 'csv': 7
[2024-03-23T06:05:36Z WARN  rust_cli] Files skipped: 8
```

### Development and Testing

Run unit tests to ensure the application functions as expected:

```bash
cargo test
```

The output is shown below:
```bash
Running tests/cli_test.rs (target/debug/deps/cli_test-8465d69ca871ed48)

running 2 tests
test test_file_count ... ok
test test_filter_by_extension ... ok

test result: ok. 2 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.04s
```