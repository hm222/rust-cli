use std::process::Command;
use std::fs::File;
use tempfile::tempdir;

#[test]
#[allow(clippy::single_char_pattern)]
fn test_file_count() {
    let dir = tempdir().unwrap();
    File::create(dir.path().join("file1.txt")).unwrap();
    File::create(dir.path().join("file2.txt")).unwrap();

    let output = Command::new("target/debug/rust-cli")
        .arg(dir.path().to_str().unwrap())
        .arg("--count")
        .output()
        .expect("Failed to execute command");
    assert!(std::str::from_utf8(&output.stderr).unwrap().contains("2"));
}

#[test]
fn test_filter_by_extension() {
    let dir = tempdir().unwrap();
    File::create(dir.path().join("file1.txt")).unwrap();
    File::create(dir.path().join("file2.rs")).unwrap();

    let output = Command::new("target/debug/rust-cli")
        .arg(dir.path().to_str().unwrap())
        .arg("--extension")
        .arg("txt")
        .output()
        .expect("Failed to execute command");
    
    let stderr = std::str::from_utf8(&output.stderr).unwrap();
    assert!(stderr.contains("matching 'txt': 1"));
    assert!(stderr.contains("skipped: 1"));
}
